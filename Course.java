public class Course  {
    private final String status;
    private final String name;
    private final int age;
    private final String [] keywords;
    public Course(String status, String name, int age, String[] keywords) {
        this.status = status;
        this.name = name;
        this.age = age;
        this.keywords = keywords;
    }
    public String getName(){
        return name;
    }
    public String getStatus() {
        return status;
    }

    public int getAge() {
        return age;
    }

    public String[] getKeywords() {
        return keywords;
    }


}
