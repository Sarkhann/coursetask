public class CourseStatus {
    public static final CourseStatus active = new CourseStatus("active");
    public static final CourseStatus deactive = new CourseStatus("deactive");
    public final String name;
    public CourseStatus(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }
    public  String findByName(){
        if (name.equals("active")) {
            return "active";
        } else if (name.equals("deactive"))
            return "deactive";
        else
            return "null";
    }
}
